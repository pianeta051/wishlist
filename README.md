**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**
---

## What is it about

This is a simple project of a simulation of a wish list. The functions of tendra are the following;

1. Add a wish.
2. Delete a wish.
3. Group wishes into categories.
4. Search wishes by category.
5. Search for wishes in the search bar.

---

## What we will use

We will work with pure Javascript without the help of any Framework working with the following:
1. Declaration of variables.
2. Get items from the .js file.
3. Array function.
4. Insertion and removal of DOM elements.
5. DOM manipulation.
6. Array iteration applying different loops
---

## Clone a repository

Clone the repository and feel free to check out all the code.
Download it by running the index.