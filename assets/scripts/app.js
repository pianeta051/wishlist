const body = document.querySelector('body');
let wishes = [];
let divWishesList = document.querySelectorAll('#wish-list div');
let divWishEmpty;
let buttonCapprice, buttonIneedIt, buttonIfindItFun;
let spanCapprice, spanIfindItFun, spanIneedIt;
let iCapprice, iIfindItFun, iIneedIt;

const CAPPRICE = 'Capprice.';
const I_NEED_IT = 'I need it.';
const I_FIND_IT_FUN = 'I find it fun.';

let selectedCategory = null;

const deleteWish = (wishId) => {
    const wishDeletedPosition = wishes.findIndex(wish => wish.id === wishId);
    wishes.splice(wishDeletedPosition, 1);
    updateWishesPage();
    updateButtonCategory();
};

const cleanDivWishList = (divWishList) => {
    const divWishes = divWishList.querySelectorAll('div');
    for (const elemento of divWishes) {
        elemento.remove();
    }
};

const cleanCategoryButtons = () => {
    const divRowCateregory = document.querySelectorAll('#wish-category div div');
    divRowCateregory.forEach(element => element.remove());
};

const cleanForm = () => {
    const modalForm = document.querySelector('.modal-list-wish');
    const inputs = modalForm.querySelectorAll('input');
    for (const input of inputs) {
        input.value = "";
    }
};


const closeFormModal = () => {
    const modalForm = document.querySelector('.modal-list-wish');
    backdrop.classList.remove('visible');
    modalForm.classList.remove('visible');
    cleanForm();
};

const container = () => {
    createSearchBar();
    createBackdrop();
    createWish();
    createWishesContainer();
    createWishCategories();

};

const createBackdrop = () => {
    const divBackdrop = createElement('div');
    divBackdrop.id = 'backdrop';
    const container = body.querySelector('.container');
    container.prepend(divBackdrop);
};

const createWish = () => {
    const divModalListWish = createElement('div');
    divModalListWish.classList.add('modal-list-wish');
    const spanCloseForm = createElement('span');
    spanCloseForm.type = 'button';
    spanCloseForm.classList.add('mr-2', 'float-right', "btn", "btn-light");
    divModalListWish.appendChild(spanCloseForm);
    const iCloseForm = createElement('i');
    iCloseForm.classList.add('far', 'fa-window-close');
    iCloseForm.setAttribute('aria-hidden', 'true');
    spanCloseForm.append(iCloseForm);

    const h2TitleModalForm = createElement('h2');
    h2TitleModalForm.innerText = "Add a wish";
    divModalListWish.appendChild(h2TitleModalForm);
    h2TitleModalForm.classList.add('title-modal-form', 'text-center', 'mt-3');
    const formModal = createElement('form');
    h2TitleModalForm.after(formModal)
    formModal.id = 'modal-form';
    const divFormContent = createElement('div');
    formModal.appendChild(divFormContent);
    divFormContent.classList.add('form-group', 'pl-3', 'pr-3');
    divFormContent.id = 'form-content';
    const labelNameForm = createElement('label');
    labelNameForm.innerText = 'Name';
    labelNameForm.for = 'name';
    divFormContent.appendChild(labelNameForm);
    const inputNameForm = createElement('input');
    inputNameForm.type = 'text';
    inputNameForm.name = 'name';
    inputNameForm.classList.add('form-control');
    labelNameForm.after(inputNameForm);
    const labelCategoryForm = createElement('label');
    labelCategoryForm.innerHTML = "Category";
    labelCategoryForm.for = 'category';
    inputNameForm.after(labelCategoryForm);
    const selectForm = createElement('select');
    selectForm.name = 'category';
    selectForm.classList.add('form-control');
    labelCategoryForm.after(selectForm);
    const optionCapriceSelectForm = createElement('option');
    optionCapriceSelectForm.innerHTML = "Capprice.";
    const optionIneedItSelectForm = createElement('option');
    optionIneedItSelectForm.innerHTML = "I need it.";
    const optionIfindItFunSelectForm = createElement('option');
    optionIfindItFunSelectForm.innerHTML = "I find it fun.";
    selectForm.appendChild(optionCapriceSelectForm);
    optionCapriceSelectForm.after(optionIneedItSelectForm);
    optionIneedItSelectForm.after(optionIfindItFunSelectForm);
    const buttonSaveForm = createElement('button');
    buttonSaveForm.innerHTML = 'Save';
    buttonSaveForm.classList.add('btn', 'btn-primary', 'float-right', 'mr-2', 'mb-3');
    formModal.append(buttonSaveForm);
    const divModalSearch = document.getElementById('modal-search');
    divModalSearch.append(divModalListWish);
};

const createBoxEmptyList = (divWishList) => {
    const divBoxEmpty = createElement('div');
    divBoxEmpty.id = 'empty-list';
    divBoxEmpty.classList.add('visible');
    const divTitleEmptyList = createElement('div');
    divTitleEmptyList.id = 'title-empty';
    const spanBoxEmptyTitle = createElement('span');
    spanBoxEmptyTitle.id = 'title-empty-list';
    spanBoxEmptyTitle.innerHTML = 'Your wish list is empty!';
    const divTextEmptyList = createElement('div');
    divTextEmptyList.id = 'text-empty-list';
    divTextEmptyList.innerHTML = 'Add some items so your friends know what to give you on your birthday';
    divBoxEmpty.appendChild(divTitleEmptyList);
    divTitleEmptyList.appendChild(spanBoxEmptyTitle);
    divBoxEmpty.append(divTextEmptyList);
    divWishList.appendChild(divBoxEmpty);
};

const createCardWish = () => {
    const cardWishList = wishes.map(wish => {
        const divWish = createElement('div');
        divWish.id = wish.id;
        divWish.classList.add('card');
        divWish.classList.add('wish');
        const spanWish = createElement('span');
        spanWish.classList.add('card-body', 'text-primary');
        const iWish = createElement('i');
        if (wish.category === CAPPRICE) {
            iWish.classList.add('fas', 'fa-ice-cream', 'float-left', 'mr-2', 'text-primary');
        } else if ((wish.category === I_NEED_IT)) {
            iWish.classList.add('fas', 'fa-bell', 'float-left', 'mr-2', 'text-primary');
        } else {
            iWish.classList.add('fas', 'fa-smile-beam', 'float-left', 'mr-2');
        }
        const ideleteWish = createElement('i');
        ideleteWish.type = 'button';
        ideleteWish.classList.add('fas', 'fa-trash', 'float-right', 'mr-2', 'btn', 'btn-outline-danger');
        spanWish.innerHTML = wish.name;

        spanWish.append(iWish, ideleteWish);
        divWish.append(spanWish);
        //cardWishList = divWish;
        ideleteWish.addEventListener('click', deleteWish.bind(this, wish.id));
        return divWish;
    });
    return cardWishList;
};
const createButtonCategory = () => {
    const rowButtonCategory = document.querySelector('#wish-category .row ');

    //adding div Capprice
    const divCapprice = createElement('div');
    divCapprice.classList.add('col-md-12');
    divCapprice.id = 'capprice';
    //adding button Capprice
    buttonCapprice = createElement('button');
    buttonCapprice.classList.add('btn', 'btn-outline-primary', 'categories', 'mt-3');
    // buttonCapprice.id = 'capprice';
    buttonCapprice.type = 'button';
    buttonCapprice.innerHTML = 'Capprice (0)';
    divCapprice.append(buttonCapprice);
    // adding span capprice
    spanCapprice = createElement('span');
    spanCapprice.classList.add('float-left');
    buttonCapprice.append(spanCapprice);
    iCapprice = createElement('i');
    iCapprice.classList.add('fas', 'fa-ice-cream');
    iCapprice.setAttribute('aria-hidden', 'true');
    spanCapprice.append(iCapprice);

    //adding div I need it
    const divIneedIt = createElement('div');
    divIneedIt.classList.add('col-md-12');
    divIneedIt.id = 'i-need-it';

    //adding button I need It
    buttonIneedIt = createElement('button');
    buttonIneedIt.classList.add('btn', 'btn-outline-primary', 'categories', 'mt-3');
    buttonIneedIt.type = 'button';
    buttonIneedIt.innerHTML = 'I need it. (0)';
    divIneedIt.append(buttonIneedIt);

    // adding span I need it
    spanIneedIt = createElement('span');
    spanIneedIt.classList.add('float-left');
    buttonIneedIt.append(spanIneedIt);
    iIneedIt = createElement('i');
    iIneedIt.classList.add('fas', 'fa-bell');
    iIneedIt.setAttribute('aria-hidden', 'true');
    spanIneedIt.append(iIneedIt);


    //adding div I find It Fun
    const divIfindItFun = createElement('div');
    divIfindItFun.classList.add('col-md-12');
    divIfindItFun.id = 'i-find-it-fun';
    //divCapprice.after(divIfindItFun);
    //adding button I find It Fun
    buttonIfindItFun = createElement('button');
    buttonIfindItFun.classList.add('btn', 'btn-outline-primary', 'categories', 'mt-3');
    buttonIfindItFun.type = 'button';
    //buttonIfindItFun.id = 'i-find-it-fun';
    buttonIfindItFun.innerHTML = 'I find It Fun. (0)';
    divIfindItFun.append(buttonIfindItFun);
    // adding span I find it fun
    spanIfindItFun = createElement('span');
    spanIfindItFun.classList.add('float-left');
    buttonIfindItFun.append(spanIfindItFun);
    iIfindItFun = createElement('i');
    iIfindItFun.classList.add('fas', 'fa-smile-beam');
    iIfindItFun.setAttribute('aria-hidden', 'true');
    spanIfindItFun.append(iIfindItFun);

    rowButtonCategory.append(divCapprice, divIneedIt, divIfindItFun);
};

const createWishCategories = () => {
    // Adding main div
    const divWishesContainer = document.querySelector('#wish-category');
    // Adding div row category div -> div
    const divRowCateregory = createElement('div');
    divRowCateregory.classList.add('row');
    //const divCategoryBox = createElement('div');

    divWishesContainer.append(divRowCateregory);
    //Adding Category Title
    const h2CategoryTitle = createElement('h2');
    h2CategoryTitle.classList.add('col-md-12');
    h2CategoryTitle.classList.add('categories-title');
    h2CategoryTitle.style.textAlign = 'center';
    h2CategoryTitle.innerHTML = 'Categories';
    divRowCateregory.append(h2CategoryTitle);
    createButtonCategory();
};

const createWishesContainer = () => {
    const divModalSearch = document.getElementById('modal-search');
    const divWishesContainer = createElement('div');
    divWishesContainer.classList.add('wishes-container', 'row', 'offset-md-2');
    divWishesContainer.id = 'id-wishes-container';
    divWishEmpty = createElement('div');
    divWishEmpty.id = 'wish-no-element';
    divWishEmpty.classList.add('col-md-6');
    if (wishes.length === 0) {
        createBoxEmptyList(divWishEmpty);
    }

    const divCardWish = createElement('div');
    divCardWish.classList.add('col-md-6');
    divCardWish.id = 'wish-list';
    divCardWish.style.display = 'none';

    const divCategoryBox = createElement('div');
    divCategoryBox.id = 'wish-category';
    divCategoryBox.classList.add('col-md-6', 'text-center');
    divWishesContainer.append(divWishEmpty, divCardWish, divCategoryBox);
    divModalSearch.after(divWishesContainer);
};

const createElement = (element) => {
    return document.createElement(element);
};

const createSectionWish = () => {
    const sectionWish = createElement('section');
    const divModalSearch = document.getElementById('modal-search');
    sectionWish.classList.add('row');
    divModalSearch.after(sectionWish);
};
const createSearchBar = () => {
    //Adding the container and some childrens
    const divContainer = createElement('div');
    body.appendChild(divContainer);
    divContainer.classList.add('container');
    const h2Title = createElement('h2');
    h2Title.classList.add('text-center', 'mt-4', 'mb-4');
    h2Title.innerHTML = 'Wish List';
    const divModalSearch = createElement('div');
    divModalSearch.id = 'modal-search';
    divModalSearch.classList.add('row', 'offset-md-2');
    divContainer.appendChild(h2Title);
    h2Title.after(divModalSearch);

    //Adding divModalSearch childrens
    const divModalButtons = createElement('div');
    divModalButtons.id = 'modal-buttons';
    divModalButtons.classList.add('col-sm-8');
    const divSearch = createElement('div');
    divSearch.classList.add('search', 'col-sm-4');
    //divSearch.id = 'search';
    divModalSearch.appendChild(divModalButtons);
    divModalButtons.after(divSearch);

    //Adding divModalSearch -> divModalButtons childrens
    const divButtons = createElement('div');
    divModalButtons.appendChild(divButtons);
    divButtons.classList.add('mt-3');
    divButtons.id = 'bar-buttons';

    //adding add / delete buttons divModalSearch -> divModalButtons - divButton childrens
    const btnAdd = createElement('button');
    btnAdd.type = 'button';
    btnAdd.innerText = "Add Wish";
    btnAdd.classList.add('btn', 'btn-outline-primary', 'mr-4');

    //Adding Add Icon
    const spanAddIcon = createElement('span');
    spanAddIcon.classList.add('float-left');
    const iAddIcon = createElement('i');
    iAddIcon.classList.add('fas', 'fa-cart-plus', 'mr-2', 'text-primary');
    btnAdd.appendChild(spanAddIcon);
    spanAddIcon.appendChild(iAddIcon);
    btnAdd.append(spanAddIcon);

    const btnDelete = createElement('button');
    btnDelete.type = 'button';
    btnDelete.innerText = "Delete All";
    btnDelete.classList.add('btn', 'btn-outline-danger');

    //Adding Delete Icon
    const spanDeleteIcon = createElement('span');
    spanDeleteIcon.classList.add('float-left');
    const iDeleteIcon = createElement('i');
    iDeleteIcon.classList.add('fas', 'fa-trash', 'mr-2');
    btnDelete.appendChild(spanDeleteIcon);
    spanDeleteIcon.appendChild(iDeleteIcon);
    btnDelete.append(spanDeleteIcon);

    divButtons.appendChild(btnAdd);
    btnAdd.after(btnDelete);

    //adding input to divModalSearch -> divSearch chindren
    const inputSearch = createElement('input');
    inputSearch.type = 'search';
    inputSearch.id = 'search';
    inputSearch.classList.add('form-control', 'mr-sm-2', 'mt-3');
    inputSearch.setAttribute('onkeyup', 'filterSearch()');
    const spanSearchIcon = createElement('span');
    spanSearchIcon.classList.add('fa', 'fa-search');

    divSearch.appendChild(spanSearchIcon);
    inputSearch.placeholder = 'Search';
    inputSearch.setAttribute('aria-label', 'search');
    spanSearchIcon.after(inputSearch);
};

const createWishList = () => {
    const divWishCointainer = document.getElementById('id-wishes-container');
    const divCardWish = createElement('div');
    divCardWish.classList.add('card', 'col-md-6');
    divCardWish.id = 'wish-list';
    const divBodyCardWish = createElement('div');
    divBodyCardWish.classList.add('card-body');
    divWishCointainer.append(divBodyCardWish);
};

const getValueInputForm = (inputName) => {
    const modalListWish = document.querySelector('.modal-list-wish');
    const input = modalListWish.querySelector(`[name="${inputName}"]`);
    return input.value.trim();
};

const filterSearch = () => {
    var input, filter, cards, card, span, txtValue;
    input = document.getElementById("search");
    filter = input.value.toUpperCase();
    cards = document.getElementById("wish-list");
    card = cards.getElementsByTagName("div");
    for (let i = 0; i < card.length; i++) {
        span = card[i].getElementsByTagName("span")[0];
        txtValue = span.textContent || span.innerText;
        (txtValue.toUpperCase().indexOf(filter) > -1) ?
        card[i].style.display = "":
            card[i].style.display = "none";
    }

};

const updateEmptyWishList = (wishList) => {
    divWishEmpty.style.display = 'block';
    wishList.style.display = 'none';
};

const updateWishList = (wishList) => {
    divWishEmpty.style.display = 'none';
    wishList.style.display = 'block';
    const cardWishes = createCardWish();
    cleanDivWishList(wishList);
    wishList.append(...cardWishes);
    updateButtonCategory();
};

const updateButtonCategory = () => {
    buttonCapprice.textContent = 'Capprice (' + wishesCounter(CAPPRICE) + ')';
    spanCapprice.append(iCapprice);
    buttonCapprice.append(spanCapprice);

    buttonIneedIt.textContent = 'I need it. (' + wishesCounter(I_NEED_IT) + ')';
    spanIneedIt.append(iIneedIt);
    buttonIneedIt.append(spanIneedIt);

    buttonIfindItFun.textContent = 'I find It Fun. (' + wishesCounter(I_FIND_IT_FUN) + ')';
    spanIfindItFun.append(iIfindItFun);
    buttonIfindItFun.append(spanIfindItFun);
};

const updateWishesPage = () => {
    const divWishList = document.getElementById('wish-list');
    (wishes.length === 0) ?
    updateEmptyWishList(divWishList):
        updateWishList(divWishList);

    divWishesList = document.querySelectorAll('#wish-list div');

};

const toggleCategory = (category) => {

    const cardWishes = document.querySelectorAll('#wish-list .card');
    if (selectedCategory === category) {
        wishes.forEach((wish, i) => {
            (wish.category !== category) ?
            cardWishes[i].classList.add('invisible'):
                cardWishes[i].classList.remove('invisible');
        });
    } else {
        updateWishesPage();
    }

};

const wishesCounter = (wishName) => {
    const wishArray = [];
    wishes.forEach(wish => {
        if (wish.category === wishName) {
            wishArray.push(wish.category);
        }
    });
    return wishArray.length;
};


container();

const addWishButton = body.querySelector('#bar-buttons .btn-outline-primary');
addWishButton.addEventListener('click', () => {
    const divBackdrop = document.querySelector('#backdrop');
    const divModalListWish = document.querySelector('.modal-list-wish');
    divBackdrop.classList.add('visible');
    divModalListWish.classList.add('visible');
    const divModalSearch = document.getElementById('modal-search');
    divModalSearch.append(divModalListWish);
});

const cancelFormButton = document.querySelector('.modal-list-wish span');
cancelFormButton.addEventListener('click', closeFormModal);

const saveButton = document.querySelector('#modal-form .btn-primary');
saveButton.addEventListener('click', evento => {
    evento.preventDefault();
    const valorInputName = getValueInputForm('name');
    const valorInputCategory = getValueInputForm('category');
    const wish = {
        name: valorInputName,
        category: valorInputCategory,
        id: wishes.length
    };
    wishes.push(wish);
    updateWishesPage();
    cleanForm();
    closeFormModal();
});

const cappriceButton = document.querySelector('#capprice button');
const iNeedItButton = document.querySelector('#i-need-it button');
const iFindItFunButton = document.querySelector('#i-find-it-fun button');

const buttonSelectedCategory = (category, buttonSelected, button2, button3) => {
    const inputSearch = document.getElementById('search');
    inputSearch.value = '';
    if (selectedCategory === category) {
        buttonSelected.classList.replace('btn-primary', 'btn-outline-primary');
        selectedCategory = null;
    } else {
        buttonSelected.classList.replace('btn-outline-primary', 'btn-primary');
        button2.classList.replace('btn-primary', 'btn-outline-primary');
        button3.classList.replace('btn-primary', 'btn-outline-primary');
        selectedCategory = category;
    }
    toggleCategory(category, buttonSelected);

};
cappriceButton.addEventListener('click', () => {
    buttonSelectedCategory(CAPPRICE, cappriceButton, iNeedItButton, iFindItFunButton);
});

iNeedItButton.addEventListener('click', () => {
    buttonSelectedCategory(I_NEED_IT, iNeedItButton, cappriceButton, iFindItFunButton);
});


iFindItFunButton.addEventListener('click', () => {
    buttonSelectedCategory(I_FIND_IT_FUN, iFindItFunButton, iNeedItButton, cappriceButton);
});

const deleteButton = document.querySelector('#bar-buttons .btn-outline-danger');
deleteButton.addEventListener('click', () => {
    wishes = [];
    updateWishesPage();
    updateButtonCategory();
});


const checkIndex = (event) => {
    console.log(Array.from(divWishesList).indexOf(event.target));
}
divWishesList.forEach(div => {
    div.addEventListener('click', checkIndex);
});